
# make the container with
docker build -t autops-dev2 .

# run with
#docker run --rm -v $PWD:$PWD -w $PWD -p 8888:8888 -it autops-dev2 bash

# example how to push to dockerhub
#docker login --username stefanprestel
#docker tag autops-dev2 stefanprestel/autops-dev2:1.0.0
#docker tag autops-dev2 stefanprestel/autops-dev2:latest
#docker push stefanprestel/autops-dev2

# example how to pull from dockerhub
#docker pull stefanprestel/autops-dev2

