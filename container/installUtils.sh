
echo "install root"

rm -rf ROOT; mkdir ROOT; cd ROOT
wget https://root.cern/download/root_v5.34.38.source.tar.gz
tar xf root_v5.34.38.source.tar.gz
cd root
./configure --prefix=$(pwd) --disable-memstat --enable-python --cxxflags="-std=c++14" --disable-asimage --cxxflags="-fPIC" --enable-soversion --enable-gsl-shared --all
make
make install

cd ../../
echo "clone repo"
git clone https://gitlab.com/prestel/autops.git
