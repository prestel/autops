// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/VetoedFinalState.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"

#include "fastjet/contrib/LundGenerator.hh"

namespace Rivet {

class MC_AUTOPS : public Analysis {
public:
  /// Constructor
  DEFAULT_RIVET_ANALYSIS_CTOR(MC_AUTOPS);

  /// Book histograms and initialise projections
  void init() {
    etaMax=4.;
    Cut nocut = Cuts::abseta < etaMax
             && Cuts::pT >= 0.*GeV
             && Cuts::E >= 0.*GeV
             && Cuts::Et >= 0.*GeV
             && Cuts::et >= 0.*GeV
             && Cuts::energy >= 0.*GeV;
    FinalState fs(nocut);
    // Projection for jets
    VetoedFinalState fsForJets(fs);
    //declare(FastJets(fsForJets, FastJets::ANTIKT, 0.1), "Jets"); //LHC
    declare(FastJets(fsForJets, FastJets::DURHAM, 0.7), "Jets"); // LEP
    book(_h_phi_vs_eta,"phi_vs_eta",
      linspace(100, 0.0, 1.0),
      linspace(100, -1.0, 1.0));
    book(_h_eta,"eta", linspace(100, 0.0, 10.0));
    book(_h_phi,"phi", linspace(100, 0.0, 2.*M_PI));
    book(_h_pT,"pT", linspace(100, 0.0, 100.0));

    book(_h_lundplane, "lundplane", linspace(100, 0.0, 5.0), linspace(100, 0.0, 10.0));

    write.open("phi_eta_pt.map");

    event_header = "========\n";
    event_footer = "";
    file_header = "phi eta pt\n";
    file_footer = "";

    write << file_header;

  }

  /// Perform the per-event analysis
  void analyze(const Event& event) {
    /*Jets jets = apply<FastJets>(event, "Jets").jetsByPt(1.*GeV); // LHC
    for (size_t i=0; i<jets.size(); ++i) {
      double phi = abs(jets[i].phi())/(2.*M_PI);
      double eta = abs(jets[i].eta())/etaMax;
      double pT  = jets[i].pT()/GeV;
      _h_phi_vs_eta->fill( phi, eta, pT);
      _h_phi->fill( abs(jets[i].phi()), 1.);
      _h_eta->fill( abs(jets[i].eta()),  1.);
      _h_pT->fill( jets[i].pT()/GeV, 1.);
    }*/
    
    write << event_header;

    //Jets jets = apply<FastJets>(event, "Jets").jets(); // LEP
    const FastJets& fastjets = apply<FastJets>(event, "Jets"); // LEP
    vector<fastjet::PseudoJet> fj = fastjets.clusterSeq()->exclusive_jets(0.001);
    //vector<fastjet::PseudoJet> fj = fastjet::sorted_by_pt(fastjets.clusterSeq()->inclusive_jets());

    for (size_t i=0; i<fj.size(); ++i) {
      if (abs(fj[i].eta()) > etaMax) continue; 
      double phi = abs(fj[i].phi())/(2.*M_PI);
      double eta = fj[i].eta()/etaMax;
      double pT  = sqrt(pow(fj[i].px(),2) + (fj[i].py(),2) )/GeV;
      _h_phi_vs_eta->fill( phi, eta, pT);
      _h_phi->fill( abs(fj[i].phi()), 1.);
      _h_eta->fill( abs(fj[i].eta()),  1.);
      _h_pT->fill( sqrt(pow(fj[i].px(),2) + (fj[i].py(),2) )/GeV, 1.);

      write << phi << " " << eta << " " << pT << std::endl;

    }

    //std::cout << jets.size() << std::endl;

    fjcontrib::LundGenerator lund;
    vector<PseudoJet> jets = fastjet::sorted_by_pt(fj);

    for (size_t i = 0; i < jets.size(); ++i) {
      vector<fjcontrib::LundDeclustering> declusts1 = lund(jets[i]);
      cout << i << " " << declusts1.size() << endl;
      for (size_t idecl = 0; idecl < declusts1.size(); ++idecl) {
         pair<double,double> coords = declusts1[idecl].lund_coordinates();
         double X = abs(coords.first);
         double Y = abs(coords.second);
         _h_lundplane->fill(X, Y);
      }

    }

    write << event_footer;

  }

  /// Normalise histograms to unit area
  void finalize() {
    const double scalefactor( crossSection() / sumOfWeights() );
    scale( _h_phi_vs_eta, scalefactor);

    write << file_footer;
    write.close();

  }

private:

  double etaMax;
  Histo2DPtr _h_phi_vs_eta;
  Histo1DPtr _h_eta, _h_phi, _h_pT;

  Histo2DPtr _h_lundplane;


  std::ofstream write;
  std::string event_header;
  std::string event_footer;
  std::string file_header;
  std::string file_footer;

};

// The hook for the plugin system
DECLARE_RIVET_PLUGIN(MC_AUTOPS);

} // namespace Rivet
