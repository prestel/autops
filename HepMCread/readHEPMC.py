#!/usr/bin/python3

import jax.numpy as jnp
import pyhepmc

###########################################################################
##  convertHEPMCeventToArray create a jax.numpy array which stores:
##  pid and momentum for particles with status code stause from HEPMC file

def convertHEPMCeventToArray(event,status=1):
  nparray = jnp.array([jnp.array([p.pid, p.momentum.e, p.momentum.x, p.momentum.y, p.momentum.z]) for p in event.particles if p.status == status])
  nparray = jnp.append(nparray, jnp.array([0,event.weights[0],0,0,0]))
  return nparray


###########################################################################
## Open HEPMC and read and convert nbEvents

with pyhepmc.open("LEP-NoHad.hepmc") as f:
    nbEvents = 10
    for x in range(nbEvents):
        event = f.read()
        print("Event number:", x )
        print(convertHEPMCeventToArray(event))

